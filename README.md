# Git repo controller #

Control your git repositories with my useful python script. Automatic apache config creation, sqlite as users db, ACL. Good for small companies and home use.

**Visit [project page](http://dev.blindage.org/projects/git-repo-controller/wiki)**