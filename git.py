#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Vladimir Smagin, 2015-2016; 21h@blindage.org http://blindage.org


import random
import subprocess
import sqlite3
import sys, os, ConfigParser
import datetime
import shutil, errno
from string import Template


def repo_make(name):
    shutil.copytree("empty_repo", repos_path + '/' + name + ".git")


def genpass():
    alphabet = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    pw_length = 16
    mypw = ""

    for i in range(pw_length):
        next_index = random.randrange(len(alphabet))
        mypw = mypw + alphabet[next_index]
    return mypw


def htpasswd_adduser(reponame, user, passwd):
    htpasswd = subprocess.Popen(["/usr/bin/htpasswd -mnb " + user + " " + passwd], stdout=subprocess.PIPE, shell=True)
    (htpasswd_output, htpasswd_err) = htpasswd.communicate()
    htpasswd_status = htpasswd.wait()
    if htpasswd_status > 0: sys.exit(1)
    apachehash = htpasswd_output.strip()
    with open(repos_path + '/' + reponame + ".passwd", "a") as htpwdhadler:
        htpwdhadler.write(apachehash + "\n")
    return 0


def htpasswd_deluser(reponame, user):
    htpasswd = subprocess.Popen(
            ["/usr/bin/htpasswd -D " + repos_path + '/' + reponame + ".passwd " + user + " 2> /dev/null"],
            stdout=subprocess.PIPE, shell=True)
    (htpasswd_output, htpasswd_err) = htpasswd.communicate()
    htpasswd_status = htpasswd.wait()
    return 0


def htpasswd_delete(reponame):
    try:
        os.remove(repos_path + '/' + reponame + ".passwd")
    except:
        print repos_path + '/' + reponame + ".passwd not exists. Nothing to delete."


def db_getuser(name):
    c.execute("select * from user where name = '" + name + "';")
    return c.fetchone()


def db_getrepo(name):
    c.execute("select * from repo where name = '" + name + "';")
    return c.fetchone()


def db_getparam(name):
    c.execute("select value from options where name = '" + name + "';")
    return str(c.fetchone()[0])


def db_setparam(name, value):
    c.execute("update options set value='" + value + "' where name = '" + name + "';")
    conn.commit()


def config_rebuild():
    file_locauth = open('templates/location-auth.tmpl')
    file_loc_nonauth = open('templates/location-nonauth.tmpl')
    file_git = open('templates/git.tmpl')
    tmpl_loc_auth = Template(file_locauth.read())
    tmpl_loc_nonauth = Template(file_loc_nonauth.read())
    tmpl_git = Template(file_git.read())
    c.execute('SELECT id,name,anon FROM repo;')
    repos_list = c.fetchall()
    loc_result = ''
    domain = db_getparam('DOMAIN')
    ssl_certfile = db_getparam('SSL_CERTFILE')
    ssl_keyfile = db_getparam('SSL_KEYFILE')
    for repo_row in repos_list:
        repo_id = repo_row[0]
        repo_name = repo_row[1]
        repo_anon = repo_row[2]
        subst_loc = {'reponame': repo_name, 'git_home': repos_path}
        if repo_anon == 1:
            loc_result = loc_result + tmpl_loc_nonauth.substitute(subst_loc)
        else:
            loc_result = loc_result + tmpl_loc_auth.substitute(subst_loc)
    subst_git = {'domain': domain, 'ssl_certfile': ssl_certfile, 'ssl_keyfile': ssl_keyfile, 'git_home': repos_path,
                 'locations': str(loc_result)}

    result = tmpl_git.safe_substitute(subst_git)
    config = open("/etc/apache2/sites-enabled/git.conf", "w")
    config.write(str(result))
    config.close()


argc = len(sys.argv)
print "GIT repos controller"
if argc < 3 or argc > 4:
    print "git.py <cmd> <params>"
    print "Commands:"
    print "\tcu - create user or update password; params: <username>"
    print "\tcup - update user password; params: <username>"
    print "\tcr - create repo; params: <reponame>"
    print "\tchrp - change repo parameters; params: <reponame>"
    print "\tcrel - create relation; params: <username> <reponame>\n"
    print "\tlist users - list users"
    print "\tlist repos - repos"
    print "\tlist rels - list relations between users and repos\n"
    print "\tdu - delete user; params: <username>"
    print "\tdr - delete repo from DB and deny access; params: <reponame>"
    print "\tdrel - delete relation: params: <username> <reponame>\n"
    print "\tregen all - regenerate passwd files"
    sys.exit(0)

# подключаем бд
# change current dir to script dir
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

conn = sqlite3.connect('git.db')
c = conn.cursor()
c2 = conn.cursor()
repos_path = db_getparam('REPOS_PATH')
# список разрешенных команд
cmds = ["cu", "cup", "cr", "crel", "chrp", "du", "dr", "drel", "list", "regen"]
cmd = sys.argv[1]

if cmd not in cmds:
    print "Not a command"
    sys.exit(1)
param1 = sys.argv[2]
if argc == 4: param2 = sys.argv[3]

# списки всей фигни в базе
if cmd == "list" and param1 == "users":
    print "All users in DB:"
    for user_row in c.execute('SELECT date,name,passwd FROM user ORDER BY name'):
        user_date = user_row[0]
        user_name = user_row[1]
        user_passwd = user_row[2]
        print "\t" + user_name + " (created " + user_date + ") password " + user_passwd
if cmd == "list" and param1 == "repos":
    print "All repos in DB:"
    for repo_row in c.execute('SELECT * FROM repo ORDER BY name'):
        repo_id = repo_row[0]
        repo_name = repo_row[1]
        repo_date = repo_row[2]
        print "\t" + repo_name + " created " + repo_date
if cmd == "list" and param1 == "rels":
    print "Relations in DB:"
    repos_list = c.execute('SELECT id,name FROM repo ORDER BY name')
    for repo_row in repos_list:
        repo_id = repo_row[0]
        repo_name = repo_row[1]
        print "\t" + repo_name + ":"
        users_list = c2.execute(
                'SELECT relation.date, user.name FROM user,relation where user.id=relation.userid and relation.repoid=' + str(
                        repo_id))
        for user_row in users_list:
            user_date = user_row[0]
            user_name = user_row[1]
            print "\t\t" + user_name + " (relations started " + user_date + ")"

# управление пользователями
if cmd == "cu":
    print "Creating new user"
    passwd = genpass()
    print "Username: " + param1 + " Password: " + passwd
    user_answer = str(db_getuser(param1))
    if user_answer != "None":
        print "User already exists!"
    else:
        dt = datetime.datetime.now()
        c.execute("INSERT INTO user (date,name,passwd) VALUES ('" + dt.strftime(
                "%d-%m-%Y") + "','" + param1 + "','" + passwd + "')")
        conn.commit()

if cmd == "cup":
    print "Updating user password in DB and all relations"
    passwd = genpass()
    print "Username: " + param1 + " Password: " + passwd
    userdata = db_getuser(param1)
    userid = userdata[0]

    relations = c.execute("select repoid from relation where userid=" + str(userid))
    for relation in relations:
        repoid = relation[0]
        related_repo = c2.execute("select name from repo where id=" + str(repoid))
        repodata = related_repo.fetchone()
        reponame = repodata[0]
        print "Updating " + reponame
        htpasswd_deluser(reponame, param1)
        htpasswd_adduser(reponame, param1, passwd)
    print "Saving new password to DB"
    c.execute("update user set passwd='" + passwd + "' where id=" + str(userid))
    conn.commit()

if cmd == "du":
    print "Deleting user and all relations"
    print "Username: " + param1
    userdata = db_getuser(param1)
    userid = userdata[0]

    relations = c.execute("select repoid from relation where userid=" + str(userid))
    for relation in relations:
        repoid = relation[0]
        related_repo = c2.execute("select name from repo where id=" + str(repoid))
        repodata = related_repo.fetchone()
        reponame = repodata[0]
        print "Deleting " + param1 + " from " + reponame
        htpasswd_deluser(reponame, param1)
    c.execute("delete from relation where userid=" + str(userid))
    c.execute("delete from user where id=" + str(userid))
    conn.commit()

# управление репозиториями
if cmd == "cr":
    print "Creating new repo"
    print "Repo name: " + param1

    # проверка не существует ли этот репозиторий в базе
    repo_answer = str(db_getrepo(param1))
    if repo_answer != "None":
        print "Repo already exists! Terminating"
        sys.exit(1)
    else:
        dt = datetime.datetime.now()
        c.execute("INSERT INTO repo (date,name) VALUES ('" + dt.strftime("%d-%m-%Y") + "','" + param1 + "')")
        conn.commit()

    if os.path.exists(repos_path + '/' + param1 + ".git"):
        print "Found repo dir. Double check it!"
    else:
        print "Creating empty repo"
        repo_make(param1)
    if os.path.exists(repos_path + '/' + param1 + ".passwd"):
        print "htpasswd file found! Removing contents!"
        htpasswd_delete(param1)
    config_rebuild()

if cmd == "chrp":
    repo_answer = str(db_getrepo(param1))
    if repo_answer != "None":
        config = ConfigParser.RawConfigParser(allow_no_value=True)
        configfilename = repos_path + '/' + param1 + '.git/config'
        config.read(configfilename)
        repo_opt_rw = raw_input("Repository read only? (y/N): ")
        if repo_opt_rw.upper() == 'Y':
            config.set('http', 'receivepack', 'false')
            c.execute("update repo set ro=1 where name='" + param1 + "';")
        else:
            config.set('http', 'receivepack', 'true')
            c.execute("update repo set ro=0 where name='" + param1 + "';")
        repo_opt_anon = raw_input("Repository with anonymous access? (y/N): ")
        if repo_opt_anon.upper() == 'Y':
            c.execute("update repo set anon=1 where name='" + param1 + "';")
        else:
            c.execute("update repo set anon=0 where name='" + param1 + "';")
        with open(configfilename, 'wb') as configfile:
            config.write(configfile)
        config_rebuild()
    else:
        print "Repo not exists! Terminating"
        sys.exit(1)

if cmd == "dr":
    print "Delete repo from DB and purge access list. Delete physical file manualy!"

    related_repo = c.execute("select id from repo where name='" + param1 + "'")
    repodata = related_repo.fetchone()
    repoid = str(repodata[0])

    htpasswd_delete(param1)

    c.execute("delete from repo where name='" + param1 + "'")
    c.execute("delete from relation where repoid='" + repoid + "'")
    conn.commit()
    config_rebuild()

# управление правами доступа
if cmd == "crel":
    print "Creating new relationship between repo '" + param2 + "' and user " + param1
    userdata = db_getuser(param1)
    repodata = db_getrepo(param2)
    dt = datetime.datetime.now()
    print "Repo ID:" + str(repodata[0]);
    print "User ID:" + str(userdata[0]);
    c.execute("INSERT INTO relation (userid,repoid,date) VALUES (" + str(userdata[0]) + "," + str(
            repodata[0]) + ",'" + dt.strftime("%d-%m-%Y") + "')")
    conn.commit()
    htpasswd_adduser(param2, param1, userdata[3])

if cmd == "drel":
    print "Deleting relationship between repo '" + param2 + "' and user " + param1
    userdata = db_getuser(param1)
    repodata = db_getrepo(param2)
    dt = datetime.datetime.now()
    print "Repo ID:" + str(repodata[0]);
    print "User ID:" + str(userdata[0]);
    c.execute("delete from relation where userid=" + str(userdata[0]) + " and repoid=" + str(repodata[0]))
    conn.commit()
    htpasswd_deluser(param2, param1)

# пересоздать все файлы паролей
if cmd == "regen" and param1 == "all":
    print('Rebuilding apache config file')
    config_rebuild()
    print "Regenerate all password files:"
    repos_list = c.execute('SELECT id,name FROM repo ORDER BY name')
    for repo_row in repos_list:
        repo_id = repo_row[0]
        repo_name = repo_row[1]
        users_list = c2.execute(
                'SELECT relation.date, user.name, user.passwd FROM user,relation where user.id=relation.userid and relation.repoid=' + str(
                        repo_id))
        for user_row in users_list:
            user_date = user_row[0]
            user_name = user_row[1]
            user_passwd = user_row[2]
            htpasswd_deluser(repo_name, user_name)
            htpasswd_adduser(repo_name, user_name, user_passwd)
            print "Regenerated: " + repo_name + ":" + user_name
c.execute("VACUUM")
conn.close()
